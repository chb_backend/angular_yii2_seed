## 1. Hosts ##

**api.hello-ay.local**


```
#!

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName api.hello-ay.local
        DocumentRoot /var/www/project_root/server/frontend/web/
        <Directory />
                Options FollowSymLinks
                AllowOverride All
        </Directory>
        <Directory /var/www/project_root/server/frontend/web/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Order allow,deny
                Allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```


**hello-ay.local**


```
#!

<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName hello-ay.local
        DocumentRoot /var/www/project_root/client/app/
        <Directory />
                Options FollowSymLinks
                AllowOverride All
        </Directory>
        <Directory /var/www/project_root/client/app/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Order allow,deny
                Allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

**Note:** Replace *project_root* to your working directory.


## 2. Initialization ##

### 2.1 DB initializtion ###

In PhpMyAdmin choose "Databases" tab. In "New database" section set databse name to **"hello_ay"** and collation to **"utf8_general_ci"**. Press "Create"

### 2.2 Initalization server-side ###

To initialize application **composer** required!

```
#!cmd

cd /server/
composer global require "fxp/composer-asset-plugin:1.0.0-beta4"
composer update
php init
```


Choose "Dev" environment


```
#!cmd

php yii migrate
```

### 2.3 Initialization client-side ###

Client-side application is already initialized.

If you have some troubles with running angular application try to reinitialize it. To initialize application **nodejs** and **npm** required!
   

**Note**: *run all commands as root*

   
First of all remove node_modules directory:


```
#!cmd

cd /client/
rm -r node_modules
```

Then you should install all required npm packages:


```
#!cmd

cd /client/
npm install
```