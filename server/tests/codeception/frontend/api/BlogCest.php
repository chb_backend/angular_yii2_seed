<?php
namespace tests\codeception\frontend;
use tests\codeception\frontend\ApiTester;

class BlogCest
{

    protected $postId;

    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function listTest(ApiTester $I)
    {
        $I->wantTo('ensure that list page works');
        $I->sendGET('blog/posts', []);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();

    }

    public function viewTest(ApiTester $I)
    {
        $I->wantTo('ensure that view page works');
        $I->sendGET('blog/posts/1', []);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['id' => 1]);

        $I->wantTo('ensure that id id not found in DB status code is 404');

        $I->sendGET('blog/posts/-1', []);
        $I->seeResponseCodeIs(404);

        $I->wantTo('pass invalid argument as id');

        $I->sendGET('blog/posts/somestring', []);
        $I->seeResponseCodeIs(404);

    }
}