(function(){
    'use strict';

    angular.module('myApp').directive('menuDir', MenuDir);

    function MenuDir(){
        return {
            templateUrl: 'common/directives/menu/views/menu.html'
        };
    }

}());