(function(){
    'use strict';

    angular.module('myApp', [
        'ngRoute',
        'myApp.home',
        'myApp.blog',
        'seo'
    ]);
}());
