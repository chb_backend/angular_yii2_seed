/**
 * Created by Lex on 16.12.2014.
 */


var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    prefix        = require('gulp-autoprefixer'),
    autowatch     = require('gulp-autowatch'),
    livereload    = require('gulp-livereload'),
    changed       = require('gulp-changed'),
    plumber       = require('gulp-plumber'),
    minifyCSS     = require('gulp-minify-css'),
    uglify        = require('gulp-uglify'),
    concat        = require('gulp-concat');


/**
 * Development Tasks
 */


// App

gulp.task('app', function () {
  return gulp.src([
      'app/app.config.js',
      'app/app.module.js',
      'app/app.router.js',
      'app/index.html'
    ])
    .pipe(plumber())
    .pipe(livereload());
});


// Views

gulp.task('views', function () {
  return gulp.src([
      'app/common/directives/**/views/*.html',
      'app/modules/**/views/*.html'
    ])
    .pipe(plumber())
    .pipe(livereload());
}); 


// SCSS-Assets

gulp.task('scss-assets', function() {
  return gulp.src('app/assets/styles/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(prefix(["last 5 version", "ie 10"]))
    .pipe(changed('app/assets/styles', {extension: '.css'}))
    .pipe(gulp.dest('app/assets/styles/'))
    .pipe(livereload());
});


// SCSS-Directives

gulp.task('scss-directives', function() {
  return gulp.src('app/common/directives/**/styles/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(prefix(["last 5 version", "ie 10"]))
    .pipe(changed('app/common/directives/', {extension: '.css', hasChanged: changed.compareLastModifiedTime}))
    .pipe(gulp.dest('app/common/directives/'))
    .pipe(livereload());
});


// SCSS-Modules

gulp.task('scss-modules', function() {
  return gulp.src('app/modules/**/styles/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(prefix(["last 5 version", "ie 10"]))
    .pipe(changed('app/modules/', {extension: '.css', hasChanged: changed.compareLastModifiedTime}))
    .pipe(gulp.dest('app/modules/'))
    .pipe(livereload());
});


// JS

gulp.task('js', function () {
  return gulp.src([
      'app/assets/js/*.js',
      'app/common/directives/**/*.js',
      'app/modules/**/*.js'
    ])
    .pipe(plumber())
    .pipe(livereload());
}); 


// Watch

var paths = {
  'app':             ['app/app.config.js', 'app/app.module.js', 'app/app.router.js', 'app/index.html'],
  'views':           ['app/common/directives/**/views/*.html', 'app/modules/**/views/*.html'],
  'scss-assets':     'app/assets/styles/*.scss',
  'scss-directives': 'app/common/directives/**/*.scss',
  'scss-modules':    'app/modules/**/styles/*.scss',
  'js':              ['app/assets/js/*.js', 'app/common/directives/**/*.js', 'app/modules/**/*.js']
};

gulp.task('watch', function(cb) {
  livereload.listen();
  autowatch(gulp, paths);
  return cb();
});


// Default Task (DEV)

gulp.task('default', [
  'scss-assets',
  'scss-directives',
  'scss-modules',
  'watch'
]);



/**
 * Production Tasks
 */


// Prod-SCSS-Base

gulp.task('prod-scss-base', function() {
  return gulp.src('app/assets/styles/*.scss')
    .pipe(plumber())
    .pipe(concat('base.scss'))
    .pipe(sass())
    .pipe(prefix(["last 5 version", "ie 10"]))
    .pipe(minifyCSS({keepBreaks: true}))
    .pipe(gulp.dest('app/assets/styles/prod'));
});


// Prod-SCSS-Style

gulp.task('prod-scss-style', function() {
  return gulp.src([
      'app/common/directives/**/styles/*.scss',
      'app/modules/**/styles/*.scss'
    ])
    .pipe(plumber())
    .pipe(concat('style.scss'))
    .pipe(sass())
    .pipe(prefix(["last 5 version", "ie 10"]))
    .pipe(minifyCSS({keepBreaks: true}))
    .pipe(gulp.dest('app/assets/styles/prod'));
});


// Prod-JS-Plugins

gulp.task('prod-js-plugins', function() {
  return gulp.src('app/assets/js/*.js')
    .pipe(plumber())
    .pipe(concat('plugins.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/assets/js/prod'));
});


// Prod-JS-App

gulp.task('prod-js-app', function() {
  return gulp.src([
      'app/common/directives/**/*.directive.js',
      'app/modules/**/controllers/*.controller.js',
      'app/modules/**/*.module.js',
      'app/modules/**/services/*.service.js'
    ])
    .pipe(plumber())
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/assets/js/prod'));
});


// Production Task (PROD)

gulp.task('prod', [
  'prod-scss-base',
  'prod-scss-style',
  'prod-js-plugins',
  'prod-js-app'
]);




