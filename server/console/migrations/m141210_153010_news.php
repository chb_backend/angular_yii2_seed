<?php

use yii\db\Schema;
use yii\db\Migration;

class m141210_153010_news extends Migration
{
    public function up()
    {
        $this->createTable('news', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'content' => Schema::TYPE_TEXT . ' NOT NULL',
            'create_time' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP'
        ]);
    }

    public function down()
    {
        echo "m141210_153010_news cannot be reverted.\n";

        return false;
    }
}
