(function(){
  'use strict';

  angular.module('myApp.blog').controller('ViewCtrl', ViewCtrl);
  ViewCtrl.$inject = ['$scope', '$routeParams', 'Post'];

  function ViewCtrl($scope, $routeParams, Post){
    $scope.post = Post.get({postId: $routeParams.postId});
  }
}());