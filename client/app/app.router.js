(function(){
    'use strict';
    angular.module('myApp').config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider.when('/blog', {
            templateUrl: 'modules/blog/views/list.html',
            controller: 'ListCtrl'
        }).when('/blog/:postId', {
            templateUrl: 'modules/blog/views/view.html',
            controller: 'ViewCtrl'
        }).when('/home', {
            templateUrl: 'modules/home/views/home.html',
            controller: 'HomeCtrl'
        }).otherwise({
            redirectTo: '/home'
        });
    }]);
}());