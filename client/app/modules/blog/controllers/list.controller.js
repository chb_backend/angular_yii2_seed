(function(){
  'use strict';

  angular.module('myApp.blog').controller('ListCtrl', ListCtrl);

  ListCtrl.$inject = ['$scope', 'Post'];

  function ListCtrl($scope, Post){
    $scope.posts = Post.query({}, function(){
        $scope.htmlReady();
    });
  }
}());