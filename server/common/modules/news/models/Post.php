<?php
/**
 * Created by PhpStorm.
 * User: Lex
 * Date: 10.12.14
 * Time: 17:48
 */

namespace common\modules\news\models;


use yii\db\ActiveRecord;

class Post extends ActiveRecord {

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'news';
    }


} 