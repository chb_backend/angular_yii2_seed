<?php
/**
 * Created by PhpStorm.
 * User: Lex
 * Date: 10.12.14
 * Time: 17:33
 */

namespace frontend\modules\blog\controllers;


use yii\rest\ActiveController;

/**
 * Class DefaultController
 * @package frontend\modules\blog\controllers
 */
class PostController extends ActiveController {

    /**
     * @var
     */
    public $modelClass = '\common\modules\news\models\Post';

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        header("Access-Control-Allow-Origin: *");
    }
} 