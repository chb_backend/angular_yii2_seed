(function(){
    'use strict';
    angular.module('myApp.blog', ['ngResource']).factory('Post', Post);

    Post.$inject = ['$resource'];

    function Post($resource){
        return $resource('http://api.hello-ay.local/blog/posts/:postId', {}, {
            query: {params: {postId: null}, isArray: true}
        });
    }

}());